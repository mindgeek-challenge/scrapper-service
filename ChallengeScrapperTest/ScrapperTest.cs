using ChallengeScrapper.Services;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.Protected;
using System.Net;

namespace ChallengeScrapperTest
{
    public class ScrapperTest
    {
        [Fact]
        public async Task TestGoodRouteFormat()
        {
            var mockFactory = this._generateMockHttpClientFactory("[{\"body\":\"Billy Crystal...\",\"cardImages\":[{\"url\":\"https://cosemgtest.blob.core.windows.net/mgtest/1a12319f-d848-401f-b7da-a4a8e828d0f2.jpg\",\"h\":1004,\"w\":768}],\"cast\":[{\"name\":\"Billy Crystal\"}],\"cert\":\"U\",\"class\":\"Movie\",\"directors\":[{\"name\":\"Andy Fickman\"}],\"duration\":5940,\"genres\":[\"Comedy\",\"Family\"],\"headline\":\"Parental Guidance\",\"id\":\"8ad589013b496d9f013b4c0b684a4a5d\",\"keyArtImages\":[{\"url\":\"https://mgtechtest.blob.core.windows.net/images/unscaled/2012/12/19/Parental-Guidance-KA-KA-to-KP3.jpg\",\"h\":169,\"w\":114}],\"lastUpdated\":\"2013-07-15\",\"quote\":\"an intriguing pairing of Bette Midler and Billy Crystal\",\"rating\":3,\"reviewAuthor\":\"Tim Evans\",\"skyGoId\":\"d1bf901693832410VgnVCM1000000b43150a____\",\"skyGoUrl\":\"http://go.sky.com/vod/content/GOPCMOVIES/RSS/Movies/content/assetId/6ba3fb6afd03e310VgnVCM1000000b43150a________/videoId/d1bf901693832410VgnVCM1000000b43150a________/content/playSyndicate.do\",\"sum\":\"66b14d5c58904900b13b404ae29eb7fe\",\"synopsis\":\"When...\",\"url\":\"http://skymovies.sky.com/parental-guidance/review\",\"videos\":[{\"title\":\"Trailer - Parental Guidance\",\"alternatives\":[{\"quality\":\"Low\",\"url\":\"http://static.video.sky.com//skymovies/2012/11/44104/44104-270p_320K_H264.mp4\"},{\"quality\":\"Medium\",\"url\":\"http://static.video.sky.com//skymovies/2012/11/44104/44104-360p_800K_H264.mp4\"},{\"quality\":\"High\",\"url\":\"http://static.video.sky.com//skymovies/2012/11/44104/44104-576p_2000K_H264.mp4\"}],\"type\":\"trailer\",\"url\":\"http://static.video.sky.com//skymovies/2012/11/44104/44104-360p_800K_H264.mp4\"}],\"viewingWindow\":{\"startDate\":\"2013-12-27\",\"wayToWatch\":\"Sky Movies\",\"endDate\":\"2015-01-21\"},\"year\":\"2012\"}]");
            var mockLogger = new Mock<ILogger<ScrapperService>>().Object;
            var service = new ScrapperService(mockFactory, mockLogger);

            var response = await service.ScrapeExternalMovieService(new CancellationToken());

            Assert.True(response.IsLeft);
        }


        [Fact]
        public async Task TestBadRouteFormat()
        {
            var mockFactory = this._generateMockHttpClientFactory("[{\"body\":\"Billy Crystal...\"]");
            var mockLogger = new Mock<ILogger<ScrapperService>>().Object;
            var service = new ScrapperService(mockFactory, mockLogger);

            var response = await service.ScrapeExternalMovieService(new CancellationToken());

            Assert.True(response.IsRight);
        }

        private IHttpClientFactory _generateMockHttpClientFactory(String expectedReponse)
        {
            var mockFactory = new Mock<IHttpClientFactory>();
            var mockHttpMessageHandler = new Mock<HttpMessageHandler>();
            mockHttpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(expectedReponse)
                });
            var client = new HttpClient(mockHttpMessageHandler.Object);
            mockFactory.Setup(_ => _.CreateClient(It.IsAny<string>())).Returns(client);

            return mockFactory.Object;
        }
    }
}