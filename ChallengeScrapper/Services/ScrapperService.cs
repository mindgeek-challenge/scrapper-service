﻿using Amazon.Auth.AccessControlPolicy;
using ChallengeScrapper.ApiModel;
using ChallengeScrapper.Exceptions;
using LanguageExt;
using System.Net.Http;
using System.Text.Json;
using Polly.Retry;
using Polly;

namespace ChallengeScrapper.Services
{
    public class ScrapperService : IScrapperService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<IScrapperService> _logger;
        private readonly Polly.Retry.AsyncRetryPolicy<HttpResponseMessage> _policy;

        public ScrapperService(IHttpClientFactory httpClientFactory, ILogger<IScrapperService> logger)
        {
            _httpClientFactory = httpClientFactory;
            _logger = logger;
            _policy = Polly.Policy
                .Handle<Exception>()
                .OrResult<HttpResponseMessage>(response => !response.IsSuccessStatusCode)
                .WaitAndRetryAsync(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(3, retryAttempt)));
        }

        public async Task<Either<IEnumerable<RawModel>, Exception>> ScrapeExternalMovieService(CancellationToken e)
        {
            var url = Environment.GetEnvironmentVariable("MovieServiceURL") ?? "https://example.com";
            _logger.LogInformation($"Scraping external movie service ${url}");

            var httpClient = _httpClientFactory.CreateClient();
            var httpRequestMessage = new HttpRequestMessage(
                HttpMethod.Get,
                url
            );

            try
            {
                var httpResponseMessage = await _policy.ExecuteAsync(async () =>
                {
                    return await httpClient.SendAsync(httpRequestMessage, e);
                });
                using var contentStream = await httpResponseMessage!.Content.ReadAsStreamAsync(e);

                var res = await JsonSerializer.DeserializeAsync<IEnumerable<RawModel>>(contentStream, cancellationToken: e);
                if (res == null)
                {
                    return new ScrapperServiceException();
                }

                return res.ToArray();
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        public async Task<Either<Stream, Exception>> ScrapeImage(String url, CancellationToken e)

        {
            _logger.LogInformation($"Scraping {url}");

            try
            {
                var httpClient = _httpClientFactory.CreateClient();
                var httpResponseMessage = await _policy.ExecuteAsync(async () =>
                {
                    return await httpClient.GetAsync(url, e);
                });

                return await httpResponseMessage.Content.ReadAsStreamAsync(e);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "Exception encountered in external service", ex.StackTrace);
                return ex;
            }
        }
    }
}
