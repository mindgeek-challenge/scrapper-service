﻿using ChallengeScrapper.ApiModel;
using LanguageExt;

namespace ChallengeScrapper.Services
{
    public interface IScrapperService
    {
        public Task<Either<IEnumerable<RawModel>, Exception>> ScrapeExternalMovieService(CancellationToken e);
        public Task<Either<Stream, Exception>> ScrapeImage(String url, CancellationToken e);
    }
}
