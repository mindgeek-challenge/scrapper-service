﻿using LanguageExt;

namespace ChallengeScrapper.Services
{
    public interface IFileStorageService
    {
        public Task<Either<String, Exception>> SaveFile(Stream file);
        public Either<FileStream, Exception> GetFile(string name);
        public bool FileExists(string name);
    }
}
