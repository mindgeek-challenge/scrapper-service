﻿using ChallengeScrapper.Configurations;
using LanguageExt;
using Microsoft.Extensions.Options;

namespace ChallengeScrapper.Services
{
    public class FileStorageService : IFileStorageService
    {
        private readonly ILogger<IFileStorageService> _logger;
        private readonly String _path;
        
        public FileStorageService(ILogger<FileStorageService> logger, IOptions<FileStorageConfiguration> fileStorage)
        {
            _logger = logger;
            _path = fileStorage.Value.SavePath;
        }

        public bool FileExists(string name)
        {
            var path = Path.Combine(_path, name);
            return File.Exists(path);
        }

        public Either<FileStream, Exception> GetFile(string name)
        {
            try
            {
                var path = Path.Combine(_path, name);
                if (File.Exists(path))
                    return File.Open(path, FileMode.Open);

                _logger.LogError($"File \"{path}\" was not found!");

                return new FileNotFoundException($"File with path \"{path}\" not found");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "File storage service exception");
                return ex;
            }
        }

        public async Task<Either<String, Exception>> SaveFile(Stream file)
        {
            try
            {
               string newName = Path.GetRandomFileName() + DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
               string savePath = Path.Combine(_path, newName);
               using FileStream fileStream = File.Open(savePath, FileMode.CreateNew);

               await file.CopyToAsync(fileStream);

               return newName;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Couldn't save file to \"{_path}\"!");
                return ex;
            }

        }
    }
}
