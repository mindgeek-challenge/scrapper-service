﻿using ChallengeScrapper.ApiModel;
using ChallengeScrapper.Model;
using ChallengeScrapper.Repository;
using ChallengeScrapper.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ChallengeScrapper.Controllers
{
    [Route("api/external/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMoviesRepository _repository;
        private readonly IFileStorageService _fileStorageService;

        public MoviesController(IMoviesRepository repository, IFileStorageService fileStorageService)
        {
            _repository = repository;
            _fileStorageService = fileStorageService;
        }

        [HttpGet]
        public async Task<ActionResult<PagedResponse<Movie[]>>> GetAllMovies([FromQuery] int? pageSize, [FromQuery] int? pageIndex, CancellationToken cancellationToken = default)
        {
            int size = pageSize ?? 50;
            int pageNr = pageIndex ?? 1;

            return Ok(new PagedResponse<Movie>()
            {
                data = await _repository.GetAllPaged(size, pageNr),
                totalCount = await _repository.GetCount(),
                pageSize = size,
                pageIndex = pageNr
            });
        }

        [HttpGet("download-artwork/{url}")]
        public async Task<IActionResult> DownloadArtwork([FromRoute] string url, CancellationToken cancellation = default)
        {
            if (!_fileStorageService.FileExists(url)) { 
                return BadRequest();
            }

            return _fileStorageService.GetFile(url)
                .Match<IActionResult>(
                    Left: res => { Response.Headers.Add("Content-Disposition", "attachment"); return Ok(res); },
                    Right: ex => StatusCode(500, "File storage unavailable")
                );
        }
    }
}
