﻿namespace ChallengeScrapper.ApiModel
{
    public record RawModel
    {
        public required String body { get; init; }
        public required IEnumerable<CardImage> cardImages { get; init; }
        public required IEnumerable<Person> cast { get; init; }
        public required String cert { get; init; }
        public required String @class { get; init; }
        public required IEnumerable<Person> directors { get; init; }
        public required Int32 duration { get; init; }
        public IEnumerable<String>? genres { get; init; }
        public required String headline { get; init; }
        public required String id { get; init; }
        public required IEnumerable<CardImage> keyArtImages { get; init; }
        public required DateOnly lastUpdated { get; init; }
        public String? quote { get; init; }
        public Int16? rating { get; init; }
        public String? reviewAuthor { get; init; }
        public String? skyGoId { get; init; }
        public String? skyGoUrl { get; init; }
        public required String sum { get; init; }
        public required String synopsis { get; init; }
        public required String url { get; init; }
        public IEnumerable<Video>? videos { get; init; }
        public ViewingWindow? viewingWindow { get; init; }
        public required String year { get; init; }

        public record CardImage(String url, Int32 h, Int32 w);
        public record Person(String name);
        public record ViewingWindow(
            DateOnly startDate,
            String wayToWatch,
            DateOnly endDate
        );

        public record Video(
            String title,
            String type,
            IEnumerable<AlternativeVideos> alternatives,
            String url
        );

        public record AlternativeVideos(
            String quality,
            String url
        );
    }
}
