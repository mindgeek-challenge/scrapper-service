﻿namespace ChallengeScrapper.ApiModel
{
    public record PagedResponse<T>
    {
        public required IEnumerable<T> data { get; init; }
        public required long totalCount { get; init; }
        public required int pageSize { get; init; }
        public required int pageIndex { get; init;}
    }
}
