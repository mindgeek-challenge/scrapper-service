﻿using ChallengeScrapper.Configurations;
using ChallengeScrapper.Model;
using LanguageExt;
using LanguageExt.Pipes;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Linq;
using static ChallengeScrapper.Model.Movie;

namespace ChallengeScrapper.Repository
{
    public class MoviesRepository : IMoviesRepository
    {
        private readonly ILogger<MoviesRepository> _logger;
        private readonly IMongoCollection<Movie> _collection;

        public MoviesRepository(ILogger<MoviesRepository> logger, IOptions<MongoConfiguration> config)
        {
            _logger = logger;
            var client = new MongoClient(config.Value.ConnectionString);
            var database = client.GetDatabase(config.Value.DatabaseName);

            _collection = database.GetCollection<Movie>(config.Value.CollectionName);

            if (!_collection.Database.ListCollectionNames().ToList().Contains(config.Value.CollectionName))
            {
                database.CreateCollection(config.Value.CollectionName);
            }
        }

        public async Task<Either<Unit, Exception>> Add(Movie movie)
        {
            _logger.LogInformation("Adding movie {id} to collection", movie.id);

            try
            {
                await _collection.InsertOneAsync(movie);
                return Unit.Default;
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        public async Task<IEnumerable<Movie>> GetAllPaged(int pageSize, int pageNumber)
        {
            _logger.LogInformation("Retrieving collection");
            
            var skipNumber = Math.Max((pageNumber - 1) * pageSize, 0);

            return await _collection.Find(_ => true).Skip(skipNumber).Limit(pageSize).ToListAsync();
        }

        public async Task<Movie?> GetById(string id)
        {
            _logger.LogInformation("Retrieving movie {id}", id);

            return await _collection.Find(p => p.id == id).FirstOrDefaultAsync();
        }

        public async Task<long> GetCount()
        {
            return await _collection.Find(_ => true).CountDocumentsAsync();
        }

        public async Task<bool> UpdatePicture(string movieId, Movie.CardImageWithCache payload)
        {
            _logger.LogInformation("Updating picture for movie {movieId}", movieId);

            var initial = await GetById(movieId);
            if (initial == null)
            {
                _logger.LogInformation("Movie {movieId} does not exist", movieId);
                return false;
            }

            var searchedImageInCardImages = initial.cardImages.FirstOrDefault(elem => elem.url == payload.url);
            var searchedImageInKeyArt = initial.keyArtImages.FirstOrDefault(elem => elem.url == payload.url);

            if (searchedImageInCardImages == null && searchedImageInKeyArt == null)
            {
                _logger.LogWarning("Movie {movieId} received invalid url for image {url}", movieId, payload.url);
                return false;
            }

            var filter = Builders<Movie>.Filter.Eq(x => x.id, movieId);

            if (searchedImageInKeyArt != null)
            {
                searchedImageInKeyArt.cacheImageUrl = payload.cacheImageUrl;
                var index = initial.keyArtImages.ToList().IndexOf(searchedImageInKeyArt);
                var update = Builders<Movie>.Update.Set(x => x.keyArtImages[index], searchedImageInKeyArt);
                await _collection.UpdateOneAsync(filter, update);

                return true;
            }
            else
            {
                searchedImageInCardImages.cacheImageUrl = payload.cacheImageUrl;
                var index = initial.cardImages.ToList().IndexOf(searchedImageInCardImages);
                var update = Builders<Movie>.Update.Set(x => x.cardImages[index], searchedImageInCardImages);
                await _collection.UpdateOneAsync(filter, update);

                return true;
            }            
        }
    }
}
