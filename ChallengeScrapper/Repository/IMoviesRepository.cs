﻿using ChallengeScrapper.Model;
using LanguageExt;
using static ChallengeScrapper.Model.Movie;

namespace ChallengeScrapper.Repository
{
    public interface IMoviesRepository
    {
        Task<IEnumerable<Movie>> GetAllPaged(int pageSize, int pageNumber);
        Task<long> GetCount();
        Task<Movie?> GetById(string id);
        Task<Either<Unit, Exception>> Add(Movie movie);
        Task<bool> UpdatePicture(string movieId, CardImageWithCache payload);
    }
}
