using ChallengeScrapper.Configurations;
using ChallengeScrapper.Events;
using ChallengeScrapper.Handlers;
using ChallengeScrapper.Repository;
using ChallengeScrapper.Services;
using MediatR;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.Configure<FileStorageConfiguration>(builder.Configuration.GetSection("FileStorage"));
builder.Services.Configure<MongoConfiguration>(builder.Configuration.GetSection("MongoConfiguration"));

builder.Services.AddSingleton<IScrapperService, ScrapperService>();
builder.Services.AddSingleton<IFileStorageService, FileStorageService>();
builder.Services.AddSingleton<IMoviesRepository, MoviesRepository>();
builder.Services.AddSingleton<IMediator, Mediator>();

builder.Services.AddTransient<INotificationHandler<ScrappingRequiredEvent>, ScrappingEventHandler>();
builder.Services.AddTransient<INotificationHandler<MovieResponseEvent>, MovieResponseHandler>();

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Movies Scrapper API", Version = "v1" });
    c.EnableAnnotations();
});
builder.Services.AddControllers();

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(
        policy =>
        {
            policy.WithOrigins("*");
        });
});


builder.Services.AddHttpClient();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors();

app.UseHttpsRedirection();
app.UseRouting();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;

    try
    {
        var mediatrService = services.GetRequiredService<IMediator>();
        Task.Run(() =>
        {
            mediatrService.Publish(new ScrappingRequiredEvent());
        });
    }
    catch (InvalidOperationException ex)
    {
        var logger = services.GetRequiredService<ILogger<Program>>();
        logger.LogError(ex, "An error occurred ", ex.Message);
    }
}

app.Run();
