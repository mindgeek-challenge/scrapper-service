﻿using System.Runtime.Serialization;

namespace ChallengeScrapper.Exceptions
{
    public class ScrapperServiceException : Exception
    {
        public ScrapperServiceException()
        {
        }

        public ScrapperServiceException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected ScrapperServiceException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
