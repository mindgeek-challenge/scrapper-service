﻿using ChallengeScrapper.ApiModel;
using MediatR;

namespace ChallengeScrapper.Events
{
    public class MovieResponseEvent : INotification
    {
        public required RawModel RawMovie { get; init; }
    }
}
