﻿namespace ChallengeScrapper.Configurations
{
    public class MongoConfiguration
    {
        public required string ConnectionString { get; set; }
        public required string DatabaseName { get; set; }
        public required string CollectionName { get; set; }
    }
}
