﻿namespace ChallengeScrapper.Configurations
{
    public class FileStorageConfiguration
    {
        public required string SavePath { get; set; }
    }
}
