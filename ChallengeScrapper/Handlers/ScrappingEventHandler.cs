﻿using ChallengeScrapper.Events;
using ChallengeScrapper.Services;
using LanguageExt;
using MediatR;

namespace ChallengeScrapper.Handlers
{
    public class ScrappingEventHandler : INotificationHandler<ScrappingRequiredEvent>
    {
        private readonly IScrapperService _scrapperService;
        private readonly IMediator _mediator;
        private readonly ILogger<ScrappingEventHandler> _logger;

        public ScrappingEventHandler(IScrapperService scrapperService, IMediator mediator, ILogger<ScrappingEventHandler> logger)
        {
            _scrapperService = scrapperService;
            _mediator = mediator;
            _logger = logger;
        }

        public async Task Handle(ScrappingRequiredEvent notification, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Received {event}", nameof(notification));


            var res = (await _scrapperService.ScrapeExternalMovieService(new CancellationToken()));

            res
            .IfLeft(async res =>
            {
                await Task.WhenAll(res.Select(async movie =>
                {
                    var @event = new MovieResponseEvent() { RawMovie = movie };
                    await _mediator.Publish(@event);
                }));
            });

            res
            .IfRight(ex =>
            {
                _logger.LogError(ex, "An error occurred ", ex.Message);
            });
        }
    }
}
