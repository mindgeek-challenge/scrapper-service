﻿using ChallengeScrapper.Events;
using ChallengeScrapper.Model;
using ChallengeScrapper.Repository;
using ChallengeScrapper.Services;
using MediatR;
using static ChallengeScrapper.Model.Movie;

namespace ChallengeScrapper.Handlers
{
    public class MovieResponseHandler : INotificationHandler<MovieResponseEvent>
    {
        private readonly ILogger<MovieResponseHandler> _logger;
        private readonly IScrapperService _scrapperService;
        private readonly IMoviesRepository _moviesRepository;
        private readonly IFileStorageService _fileStorageService;

        public MovieResponseHandler(ILogger<MovieResponseHandler> logger, IScrapperService scrapperService, IMoviesRepository moviesRepository, IFileStorageService fileStorageService)
        {
            _logger = logger;
            _scrapperService = scrapperService;
            _moviesRepository = moviesRepository;
            _fileStorageService = fileStorageService;
        }

        public async Task Handle(MovieResponseEvent notification, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Received {event}", nameof(notification));

            var movieInDb = await _moviesRepository.GetById(notification.RawMovie.id);

            if (movieInDb == null)
            {
                movieInDb = Movie.FromRawModel(notification.RawMovie);
                await _moviesRepository.Add(movieInDb);
            }

            var f = async (CardImageWithCache image) =>
            {
                if (image.cacheImageUrl != null && _fileStorageService.FileExists(image.cacheImageUrl))
                {
                    _logger.LogInformation("Cached image {url}", image.url);
                    return;
                }

                (await _scrapperService.ScrapeImage(image.url, cancellationToken))
                    .IfLeft(async imageStream =>
                        (await _fileStorageService.SaveFile(imageStream))
                            .IfLeft(async res =>
                            {
                                image.cacheImageUrl = res;
                                await _moviesRepository.UpdatePicture(notification.RawMovie.id, image);
                            })
                        );
            };

            await Task.WhenAll(movieInDb.cardImages.Select(f));
            await Task.WhenAll(movieInDb.keyArtImages.Select(f));
        }
    }
}
