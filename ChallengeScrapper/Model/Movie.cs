﻿using ChallengeScrapper.ApiModel;
using LanguageExt;
using MongoDB.Bson;
using static ChallengeScrapper.ApiModel.RawModel;

namespace ChallengeScrapper.Model
{
    public class Movie
    {
        public ObjectId _Id { get; set; }

        public required String body { get; init; }
        public required IList<CardImageWithCache> cardImages { get; set; }
        public required IEnumerable<Person> cast { get; init; }
        public required String cert { get; init; }
        public required String @class { get; init; }
        public required IEnumerable<Person> directors { get; init; }
        public required Int32 duration { get; init; }
        public IEnumerable<String>? genres { get; init; }
        public required String headline { get; init; }
        public required String id { get; init; }
        public required IList<CardImageWithCache> keyArtImages { get; set; }
        public required DateOnly lastUpdated { get; init; }
        public String? quote { get; init; }
        public Int16? rating { get; init; }
        public String? reviewAuthor { get; init; }
        public String? skyGoId { get; init; }
        public String? skyGoUrl { get; init; }
        public required String sum { get; init; }
        public required String synopsis { get; init; }
        public required String url { get; init; }
        public IEnumerable<Video>? videos { get; init; }
        public ViewingWindow? viewingWindow { get; init; }
        public required String year { get; init; }

        public record CardImageWithCache : CardImage
        {
            public String? cacheImageUrl { get; set; }

            public CardImageWithCache(String url, Int32 h, Int32 w) : base(url, h, w) { }
            public CardImageWithCache(CardImage image) : base(image.url, image.h, image.w) { }
        }

        public static Movie FromRawModel(RawModel rawModel)
        {
            return new Movie()
            {
                body = rawModel.body,
                cardImages = rawModel.cardImages.Select(e => new CardImageWithCache(e)).ToList(),
                cast = rawModel.cast,
                cert = rawModel.cert,
                @class = rawModel.@class,
                directors = rawModel.directors,
                duration = rawModel.duration,
                genres = rawModel.genres,
                headline = rawModel.headline,
                id = rawModel.id,
                keyArtImages = rawModel.keyArtImages.Select(e => new CardImageWithCache(e)).ToList(),
                lastUpdated = rawModel.lastUpdated,
                quote = rawModel.quote,
                rating = rawModel.rating,
                reviewAuthor = rawModel.reviewAuthor,
                skyGoId = rawModel.skyGoId,
                skyGoUrl = rawModel.skyGoUrl,
                sum = rawModel.sum,
                synopsis = rawModel.synopsis,
                url = rawModel.url,
                videos = rawModel.videos,
                viewingWindow = rawModel.viewingWindow,
                year = rawModel.year,
            };
        }
    }
}
